# Google Maps Platform Samples

GitBook site: [https://windysky912.gitlab.io/gmp-sample/](https://windysky912.gitlab.io/gmp-sample/)

## API Combo

- [Facilities with travel time](https://windysky912.gitlab.io/gmp-sample/codes/api-combo/facilities-with-travel-time.html?demo=1)
- Street view on road

## Best Parctices

- [Places Autocomplete](https://windysky912.gitlab.io/gmp-sample/codes/best-practices/places-autocomplete.html?demo=1)
- [Map Load](https://windysky912.gitlab.io/gmp-sample/codes/best-practices/map-load.html?demo=1)

## Solution Phototype

- [Delivery route optimization](https://windysky912.gitlab.io/gmp-sample/codes/solution-prototype/delivery-route-optimization.html?demo=1)

## API Key

- Please replace YOUR_API_KEY to your Google Maps API key in the `<script>` tag or Static Maps url.

```
<!-- Please replace YOUR_API_KEY to your Google Maps API key. -->
<script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&callback=initMap" async defer></script>
```

```
<!-- Please replace YOUR_API_KEY to your Google Maps API key. -->
<img id="sevenSMap"
    src="http://maps.google.com/maps/api/staticmap?key=YOUR_API_KEY&zoom=16&size=512x512&center=25.034161,121.561019&markers=label:1|25.034268,121.561017&markers=label:2|25.034276,121.560048&markers=label:3|25.033715,121.55769&markers=label:4|25.032004,121.558744"
    class="smap" onclick="loadMap(event, 'seven')" />
```

- If you don't have an Google Maps API key, please follow this documentation to get one: [Get an API Key](https://developers.google.com/maps/documentation/javascript/get-api-key?hl=zh-tw)
