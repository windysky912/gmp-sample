# Places Autocomplete

## 目的

用戶在操作註冊會員、網購結帳或叫車等服務時，往往必須輸入一長串地址或地點名稱，操作起來花費時間，且無法確認用戶輸入的是準確的資訊。透過 Places API 的 Places Autocomplete 功能，依據用戶輸入的關鍵字，從 Google Maps 地點資料庫中比對出相符合的店家、地址與景點資料，作為地點建議選項提供用戶選擇，可加速操作流程，並確保資訊完整且準確。

那該如何在服務中加入 Places Autocomplete 功能，且以最節省成本的方式使用此功能呢？

## 實作與功能

Places API 中，提供三種方式實作 Places Autocomplete 功能：

### Autocomplete Widgets

![Places Autocomplete Widget 輸入文字，顯示包含地點建議選項的下拉選單](../../images/places-autocomplete-widget-1.png)
![Places Autocomplete Widget 選擇地點，取得地點資料，如地點的座標來在地圖上標記地點](../../images/places-autocomplete-widget-2.png)

- 透過 `Autocomplete` 在文字輸入框上建立 Autocomplete Widget。

```js
    var input = document.getElementById('autocompleteTextField');

    autocomplete = new google.maps.places.Autocomplete(input, options);
```

- 內建 UI 元件，當用戶輸入文字後，顯示一下拉選單，列出符合輸入文字的地點建議選項。
- 用戶選擇地點建議選項後，觸發 `place_changed` 事件，並可用 `Autocomplete.getPlace()` 取得該地點的詳細資訊。

```js
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
    
        ...
    });
```

更詳細的功能與設置，請參考[此文件](https://developers.google.com/maps/documentation/javascript/places-autocomplete#add_autocomplete)。

### SearchBox

![Places Autocomplete SearchBox 輸入文字，顯示包含建議關鍵字與地點選項的下拉選單](../../images/places-autocomplete-searchbox-1.png)
![Places Autocomplete SearchBox 選擇關鍵字，以關鍵字進行地點搜索，標記多個符合搜索條件的地點位置](../../images/places-autocomplete-searchbox-2.png)
![Places Autocomplete SearchBox 輸入文字，顯示包含建議關鍵字與地點選項的下拉選單](../../images/places-autocomplete-searchbox-3.png)
![Places Autocomplete SearchBox 選擇地點，取得地點資料，如地點的座標來在地圖上標記地點](../../images/places-autocomplete-searchbox-4.png)

- 透過 `SearchBox` 在文字輸入框上建立 Autocomplete SearchBox。

```js
    var input = document.getElementById('autocompleteTextField');
    
    autocomplete = new google.maps.places.SearchBox(input, options);
```

- 內建 UI 元件，當用戶輸入文字後，顯示一下拉選單，列出符合輸入文字的**關鍵字**與地點建議選項。
- 用戶選擇建議選項後，觸發 `place_changed` 事件，並可用 `searchBox.getPlaces()` 取得一或多個地點資訊：
    - 若選擇地點建議選項，地點資訊包含該地點詳細資料。
    - 若選擇關鍵字建議選項，地點資訊包含符合條件的多個地點資料。

```js
    autocomplete.addListener('place_changed', function () {
        var places = searchBox.getPlaces();
    
        ...
    });
```

更詳細的功能與設置，請參考[此文件](https://developers.google.com/maps/documentation/javascript/places-autocomplete#places_searchbox)。

### AutocompleteService

![Places Autocomplete AutocompleteService 依據查詢的文字，回傳地點建議清單](../../images/places-autocomplete-autocompleteservice-1.png)
![Places Autocomplete AutocompleteService 自行實作 UI，輸入文字，顯示包含地點建議選項的下拉選單](../../images/places-autocomplete-autocompleteservice-2.png)
![Places Autocomplete AutocompleteService 自行實作 UI，選擇地點，取得地點資料，如地點的座標來在地圖上標記地點](../../images/places-autocomplete-autocompleteservice-3.png)

- 透過 `AutocompleteService` 建立 Autocomplete 服務。

```js
    var service = new google.maps.places.AutocompleteService();
```

- 透過 `service.getPlacePredictions` 來查詢地點建議清單。

```js
    autocompleteService.getPlacePredictions({ input: value }, displaySuggestions);
```

- 未內建 UI 元件，當用戶輸入文字後，須自行實作一下拉選單，顯示出符合輸入文字的地點建議選項，並將地點編號 `place_id` 暫存於選項中。
- 自行監控用地點建議選項的 `click` 事件，在用戶選擇時觸發，並從建議選項中取得地點編號 `place_id` 來查詢該地點的詳細資訊。

```js
    function displaySuggestions(predictions, status) {
        if (status !== google.maps.places.PlacesServiceStatus.OK &&
            status !== google.maps.places.PlacesServiceStatus.ZERO_RESULTS) {
            window.alert('Places autocomplete request failed due to ' + status);
            return;
        }
        // Keep last suggestion list if there is no matched suggestion places.
        if (!predictions) {
            return;
        }
    
        // Handle autocomplete results to show a suggestion list.
        ...
    
        // Bind click event on each suggestion place item.
        ...
    
        autocompleteItems.forEach(function (autocompleteItem) {
            ...
    
            // Get name, coordinate and address of selected place by Places API - Places Details.
            autocompleteItem.addEventListener("click", getSelectedPlace);
        });
    }
    
    function getSelectedPlace() {
        var placesService = new google.maps.places.PlacesService(map);
    
        // Get place_id of selected place.
        var selectedText = this.querySelector('.autocomplete-text').textContent;
        var place_id = this.getAttribute('data-place-id');
    
        // Call Places API - Places Details.
        // Set fields param of Places Details to return only name, coordinate and address of selected place.
        var placesDetailsReq = {
            placeId: place_id,
            fields: ['name', 'formatted_address', 'geometry']
        };
        placesService.getDetails(placesDetailsReq, function (place, status) {
            // Display place data.
            ...
        });
    }
```

更詳細的功能與設置，請參考[此文件](https://developers.google.com/maps/documentation/javascript/places-autocomplete#place_autocomplete_service)。

## 計費方式

用戶操作 Places Autocomplete 功能，會產生以下一系列費用：
1. **查詢建議選項的費用**：
用戶每次輸入文字，更新下拉選單的建議選項，就會產生一次查詢費用。
2. **查詢選擇地點的費用**：
用戶選擇一建議選項，取得該地點的地點資料，產生一次查詢費用。
3. **地點資料的費用**：
地點資料分為 Basic、Contact、Atmosphere 三類型，分別有不同的費用，當取回地點資料時，依據取回的地點資料所包含的資料類型，產生一次資料費用。

### 依請求計費

依據**查詢建議選項的請求數量**來計算 “查詢建議選項的費用”，完整操作產生費用如下：
1. **查詢建議選項的費用**：
Autocomplete - Per Request \* N (**N 為用戶輸入文字的字元數**)
2. **查詢選擇地點的費用**：
Places Details \* 1
3. **地點資料的費用**：
Basic Data \* 1 + Contact Data \* 1 (**若包含此類型資料**) + Atmosphere Data \* 1 (**若包含此類型資料**)

各項計費項目(SKU)的費用如下：(單位：美金)

| 計費項目(SKU)               | 1 ~ 100,000 (次) | 100,001 ~ 500,000 (次) |
| ---                         | --:              | --:                    |
| Autocomplete - Per Request  |          0.00283 |                0.00227 |
| Places Details              |            0.017 |                 0.0136 |
| Basic Data                  |                0 |                      0 |
| Contact Data                |            0.003 |                 0.0024 |
| Atmosphere Data             |            0.005 |                  0.004 |

500,001 次以上的單價，請[聯絡銷售人員](https://cloud.google.com/contact/)。

### 依工作階段計費

#### 自動完成工作階段(Autocomplete sessions)

在完整的 Places Autocomplete 操作中，包含多次 `查詢建議選項` 與一次 `查詢選擇地點` 的動作。
在每次要進行這一連串查詢時，使用同一個 session token，可以讓這些動作被視為同一個工作階段，並以工作階段數量來計費。如此一來，在同一個工作階段裡，不管發送多少個查詢建議選項的請求，都會以相同的費用收費。

- Autocomplete Widget 自動使用工作階段模式來提供服務。
- SearchBox 不支援工作階段模式。
- AutocompleteService 預設依請求數量計費，可自行在請求中加入 session token 來依工作階段數量計費：

```js
    // Create a new session token.
    var sessionToken = new google.maps.places.AutocompleteSessionToken();
    // Pass the token to the autocomplete service.
    var autocompleteService = new google.maps.places.AutocompleteService();
    autocompleteService.getPlacePredictions({
      input: 'Coffee near Lond',
      sessionToken: sessionToken
    }, displaySuggestions);
```

#### 工作階段費用

依據**工作階段的數量**來計算 “查詢建議選項的費用”，完整操作產生費用如下：
1. **查詢建議選項的費用**：
Autocomplete (included with Places Details) - Per Session \* 1 (**不管查詢了幾次建議選項，皆視為同一工作階段**)
2. **查詢選擇地點的費用**：
Places Details \* 1
3. **地點資料的費用**：
Basic Data \* 1 + Contact Data \* 1 (**若包含此類型資料**) + Atmosphere Data \* 1 (**若包含此類型資料**)

各項計費項目(SKU)的費用如下：(單位：美金)

| 計費項目(SKU)                                             | 1 ~ 100,000 (次) | 100,001 ~ 500,000 (次) |
| ---                                                       | --:              | --:                    |
| Autocomplete (included with Places Details) - Per Session |                0 |                      0 |
| Places Details                                            |            0.017 |                 0.0136 |
| Basic Data                                                |                0 |                      0 |
| Contact Data                                              |            0.003 |                 0.0024 |
| Atmosphere Data                                           |            0.005 |                  0.004 |

500,001 次以上的單價，請[聯絡銷售人員](https://cloud.google.com/contact/)。

#### 工作階段逾時費用

當使用工作階段的方式來查詢建議選項時，若用戶未選擇建議選項，且 **6 分鐘**內無後續操作，則工作階段將會逾時，產生工作階段逾時費用：
- Autocomplete without Places Details - Per Session

計費項目(SKU)的費用如下：(單位：美金)

| 計費項目(SKU)                                     | 1 ~ 100,000 (次) | 100,001 ~ 500,000 (次) |
| ---                                               | --:              | --:                    |
| Autocomplete without Places Details - Per Session |            0.017 |                 0.0136 |

500,001 次以上的單價，請[聯絡銷售人員](https://cloud.google.com/contact/)。

### SearchBox 計費方式

SearchBox 使用內建 UI 提供關鍵字與地點建議選項，產生以下費用：
1. **查詢建議選項的費用**：
Query Autocomplete - Per Request \* N (**N 為用戶輸入文字的字元數**)
2. **查詢選擇地點的費用**：(用戶選擇**地點**建議選項)
Places Details \* 1
3. **以關鍵字搜索地點的費用**：(用戶選擇**關鍵字**建議選項)
Places - Text Search \* 1
4. **地點資料的費用**：
Basic Data \* 1 + Contact Data \* 1 (**若包含此類型資料**) + Atmosphere Data \* 1 (**若包含此類型資料**)

各項計費項目(SKU)的費用如下：(單位：美金)

| 計費項目(SKU)                    | 1 ~ 100,000 (次) | 100,001 ~ 500,000 (次) |
| ---                              | --:              | --:                    |
| Query Autocomplete - Per Request |          0.00283 |                0.00227 |
| Places Details                   |            0.017 |                 0.0136 |
| Places - Text Search             |            0.032 |                 0.0256 |
| Basic Data                       |                0 |                      0 |
| Contact Data                     |            0.003 |                 0.0024 |
| Atmosphere Data                  |            0.005 |                  0.004 |

500,001 次以上的單價，請[聯絡銷售人員](https://cloud.google.com/contact/)。

更詳細的計費資訊，請參考[此文件](https://developers.google.com/maps/billing/gmp-billing#places-product)。

## 優化建議

### 取得特定的地點資料

地點資料分為以下三個類型：(各類型完整的資料欄位資訊，請參考[此文件](https://developers.google.com/maps/documentation/javascript/place-data-fields))
- Basic Data：
包含地點編號(place_id)、地點名稱(name)、地點類型(type)、地址(formatted_address)、座標(geometry) 等資料。
- Contact Data：
包含電話(formatted_phone_number)、營業時間(opening_hours)、網站(website) 等資料。
- Atmosphere Data：
包含評等(rating)、評論(review)、總評論數(user_ratings_total) 等資料。

因取得不同類型的資料，會產生不同的資料費用，要節省不必要的花費，可以在查詢地點資料時，指定只回傳特定的資料欄位：
(如只需取得地點的座標(geometry)資訊)
- Autocomplete widget：使用 `autocomplete.setFields(['geometry'])` 來指定回傳的資料欄位。

```js
    var input = document.getElementById('autocompleteTextField');
    
    autocomplete = new google.maps.places.Autocomplete(input, options);
    
    autocomplete.setFields(['geometry']);
    
    autocomplete.addListener('place_changed', function () {
        var place = autocomplete.getPlace();
    
        ...
    });
```

- SearchBox：無法指定回傳的地點資料欄位，每次都會回傳所有可用的資料欄位。
- AutocompleteService：在呼叫 `PlacesService.getDetails(request, callback)` 的 `request` 中，指定 `fields` 參數。

```js
    var placesService = new google.maps.places.PlacesService(map);
    
    var placesDetailsReq = { placeId: place_id, fields: ['geometry'] };
    placesService.getDetails(placesDetailsReq, function (place, status) {
            // Display place data.
            ...
    });
```

### Per request vs Per Session

大部分情況下，我們透過 Places Autocomplete 來加速查詢地點資料，在用戶選擇地點建議選項時，會觸發一 Places Details 請求，查詢目標地點的地點資料。

在這樣的情境中，使用工作階段模式，可以節省掉 **查詢建議選項的費用**：
- 依請求數量計費：

```
Autocomplete - Per Request * N + Places Details * 1 + (Basic Data + Contact Data + Atmosphere Data) * 1
= 0.00283 * N + 0.017 + (0 + 0.003 + 0.005)
```

- 依工作階段計費：

```
Autocomplete (included with Places Details) - Per Session * 1 + Places Details * 1 + (Basic Data + Contact Data + Atmosphere Data) * 1
= 0 + 0.017 + (0 + 0.003 + 0.005)
```

### 與 Geocoding API 等 Google Maps APIs 一起使用

當我們請用戶輸入地址來查詢該地址所以在位置時，避免用戶輸入不完整的地址，可使用 Places Autocomplete 提供地址建議選項，當用戶選擇一建議地址後，透過選項中的 place_id，以 Geocoding API 查詢出該地址所在座標。

在這樣的情境中，若使用工作階段模式，因後續未呼叫 Placs Details 來查詢地點資料，會產生工作階段逾時的收費項目：
- 依請求數量計費：

```
Autocomplete - Per Request * N + Geocoding * 1
= 0.00283 * N + 0.005
```

- 依工作階段計費：

```
Autocomplete without Places Details - Per Session * 1 + Geocoding * 1
= 0.017 + 0.005
```

由此可見，當用戶輸入 **6 個字元** 以內，就能找到欲查詢的建議地址選項時，不使用工作階段模式是更划算的。

另外，當使用量龐大，將大用量折扣考量進去時，則可依用戶每次操作輸入字數與用戶操作次數來計算使用哪種方式較划算。

例如，每次操作輸入字數 30 字，操作次數超過 539,412 次後，不使用工作階段模式費用就比較便宜了：
![Autocomplete - Per Request vs Autocomplete without Places Details - Per Session](../../images/places-autocomplete-request-vs-session.png)

### AutocompleteSerivce 加入 debounce 與 minChar 機制

- debounce function

當用戶操作 Places Autocomplete 時，通常會連續輸入一段欲查詢的關鍵字，再從建議選項中選擇目標地點。

因此，可使用 debounce function 來限制 Places Autocomplete 查詢建議選項的請求數量，當用戶連續輸入文字時，不在每次鍵入文字時都發送查詢建議選項的請求，而是設定一固定的等待時間，時間到後才發送一次請求，以減少用戶輸入關鍵字途中所產生的不必要查詢請求。

- minChar

當用戶操作 Places Autocomplete 時，在未輸入足夠的關鍵字前，通常查詢出來的建議選項不會包含用戶的目標地點。

因此，可設定用戶要輸入超過最少文字量(minChar)後，才發送 Places Autocomplete 查詢建議選項的請求，以減少未輸入足夠關鍵字前所產生的不必要查詢請求。

```js
    var DEBOUNCE_WAIT = 150;
    var MINCHAR = 2;
    
    function debounce(func, wait, immediate) {
    	var timeout;
    	return function() {
    		var context = this, args = arguments;
    		var later = function() {
    			timeout = null;
    			if (!immediate) func.apply(context, args);
    		};
    		var callNow = immediate && !timeout;
    		clearTimeout(timeout);
    		timeout = setTimeout(later, wait);
    		if (callNow) func.apply(context, args);
    	};
    };
    
    autocompleteInput.addEventListener("input", debounce(function () {
        
        ...
    
        if (value.length > MINCHAAR) {
            // Call Places Autocomplete to get suggestion places.
            autocompleteService.getPlacePredictions({
                input: value
            }, displaySuggestions);
        }
    
        ...
    
    }, DEBOUNCE_WAIT));
```

## 總結

1. 若透過 Places Autocomplete 來加速查詢地點資料，建議使用 `Autocomplete Widget` 或 `AutocompleteService.getPlacePredictions()` 加上工作階段模式。
2. 若透過 Places Autocomplete 來加速以下情境，建議使用 `AutocompleteService.getPlacePredictions()` 加上 `Debounce` 與 `minChars` 機制。
    - Geocoding API：輸入地址查詢經緯度
    - Direction API：輸入起訖位置來進行路徑規劃
    - Distance Matrix API：輸入起訖位置來計算行駛距離與時間
    - 存取建議選項中的資料(如 place_id 或 選項文字) 到其他服務中使用
3. 若 Places Autocomplete 需提供關鍵字與地點建議選項，建議不使用 `SearchBox`，使用 `AutocompleteService.getQueryPredictions()` 加上 `Debounce` 與 `minChars` 機制，來查詢關鍵字與地點建議選項。
