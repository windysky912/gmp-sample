# Summary

* [Google Maps Platform Samples](README.md)

## API Combo

* [Facilities with travel time](articles/api-combo/facilities-with-travel-time.md)

## Best Practices

* [Map Load](articles/best-practices/map-load.md)
* [Places Autocomplete](articles/best-practices/places-autocomplete.md)

## Solution Prototype

* [Delivery route optimization](articles/solution-prototype/delivery-route-optimization.md)
